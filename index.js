'use strict';

const AWS = require('aws-sdk');
const cf = require('aws-cloudfront-sign')

exports.handler = (event, context, callback) => {
    
    //Get contents of response
    const response = event.Records[0].cf.response;
    
    let keyPair = 'REPLACE-ME';

    var options = {keypairId: keyPair , privateKeyPath: './private-key.pem'}
    var signedCookies = cf.getSignedCookies('https://*', options);
    console.log('Signed cookies: ' + JSON.stringify(signedCookies));
     
    const headers = response.headers;
        
        //Set new headers 
    headers['strict-transport-security'] = [{key: 'Strict-Transport-Security', value: 'max-age= 63072000; includeSubdomains; preload'}]; 
    headers['content-security-policy'] = [{key: 'Content-Security-Policy', value: "default-src 'none'; img-src 'self'; script-src 'self'; style-src 'self'; object-src 'none'"}]; 
    headers['x-content-type-options'] = [{key: 'X-Content-Type-Options', value: 'nosniff'}]; 
    headers['x-frame-options'] = [{key: 'X-Frame-Options', value: 'DENY'}]; 
    headers['x-xss-protection'] = [{key: 'X-XSS-Protection', value: '1; mode=block'}]; 
    headers['referrer-policy'] = [{key: 'Referrer-Policy', value: 'same-origin'}]; 
    headers['x-lambda-version'] = [{key: 'X-Lambda-Version', value: context.functionVersion}]; 
    headers['set-cookie'] = [{key: 'Set-Cookie', value: 'CloudFront-Policy=' + signedCookies['CloudFront-Policy']},
               {key: 'Set-Cookie', value: 'CloudFront-Signature=' + signedCookies['CloudFront-Signature']},
               {key: 'Set-Cookie', value: 'CloudFront-Key-Pair-Id=' + signedCookies['CloudFront-Key-Pair-Id']}]; 
     
  
    response.status = 200;
    response.statusDescription = 'OK';
    response.body = 'Body generation example';
        

    //Return modified response
    callback(null, response);
};